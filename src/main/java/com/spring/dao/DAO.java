package com.spring.dao;

import java.sql.Date;
import java.util.List;

import javax.sql.DataSource;

import com.spring.model.Customers;
import com.spring.model.Orders;

public interface DAO {
	public void setDataSource(DataSource dataSource);

	public List<Customers> getCustomerByOrderDate(Date orderDate);

	public List<Orders> getOrdersBelongTo(String custName);

	public int updateOrderAmount(int orderId, double newAmount);

	public Customers getCustomer(int custID);
	
	public List<Orders> getTotal();
}
