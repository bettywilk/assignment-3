package com.spring.service;

import java.sql.Date;
import java.util.List;

import com.spring.dao.DAO;
import com.spring.model.Customers;
import com.spring.model.Orders;

public class AppServiceImpl implements AppService {
	private DAO appDao;

	public DAO getAppDao() {
		return appDao;
	}

	public void setAppDao(DAO appDao) {
		this.appDao = appDao;
	}

	@Override
	public List<Customers> getCustomerByOrderDate(Date orderDate) {
		return appDao.getCustomerByOrderDate(orderDate);
	}

	@Override
	public List<Orders> getOrdersBelongTo(String custName) {
		return appDao.getOrdersBelongTo(custName);
	}

	@Override
	public int updateOrderAmount(int orderId, double newAmount) {
		return appDao.updateOrderAmount(orderId, newAmount);
	}
	
	@Override
	public List<Orders> getTotal(){
		return appDao.getTotal();
	}
}
